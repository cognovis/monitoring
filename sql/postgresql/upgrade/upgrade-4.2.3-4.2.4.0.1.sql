-- 
-- 
-- 
-- Copyright (c) 2015, cognovís GmbH, Hamburg, Germany
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- 
-- @author <yourname> (<your email>)
-- @creation-date 2013-01-19
-- @cvs-id $Id$
--

--SELECT acs_log__debug('/packages/intranet-core/sql/postgresql/upgrade/upgrade-4.2.3-4.2.4.0.1.sql','');

alter table ad_monitoring_top_df_item alter column filesystem type text;
alter table ad_monitoring_top_df_item alter column mounted type text;
alter table ad_monitoring_top_df_item alter column size type text;
alter table ad_monitoring_top_df_item alter column used type text;
alter table ad_monitoring_top_df_item alter column used_percent type text;
alter table ad_monitoring_top_df_item alter column avail type text;
